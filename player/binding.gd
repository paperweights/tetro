class_name Binding
extends Resource

enum Value {
	RELEASED = 0,
	HELD,
	PRESSED,
}

@export var key: Key
var _value: Value


func handle(ev: InputEventKey) -> bool:
	if ev.keycode != key:
		return false
	if ev.echo:
		_value = Value.HELD
	elif ev.pressed:
		_value = Value.PRESSED
	else:
		_value = Value.RELEASED
	return true


func held() -> float:
	return float(!_value == Value.RELEASED)

func pressed() -> bool:
	return _value == Value.PRESSED
