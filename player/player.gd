extends CharacterBody2D


class Jump:
	var velocity: float
	var gravity: float

	func _init(height: float, distance: float, speed: float):
		velocity = -2 * height * speed / distance
		gravity = 2 * height * speed ** 2 / distance ** 2
		return


const SPEED: float = 64.0 # vx

@export var _controls: Array[Binding] = []

var _jump_bicc := Jump.new(16, 16, SPEED)
var _jump_smol := Jump.new(8, 8, SPEED)

@onready var _sprite := $AnimatedSprite2D as AnimatedSprite2D


func _unhandled_key_input(event: InputEvent):
	if !event is InputEventKey:
		return
	for control in _controls:
		if control.handle(event):
			return
	return


func _physics_process(delta: float):
	# Add the gravity.
	if not is_on_floor():
		if velocity.y < 0 and !_controls[0].held():
			velocity.y += _jump_smol.gravity * delta
		else:
			velocity.y += _jump_bicc.gravity * delta
	if _controls[0].pressed() and is_on_floor():
		velocity.y = _jump_bicc.velocity
	var direction = _get_horizontal_direction()
	if direction:
		velocity.x = direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
	move_and_slide()
	_update_animation()
	return


func _get_horizontal_direction() -> float:
	return -_controls[1].held() + _controls[3].held()


func _update_animation():
	if is_on_floor():
		if velocity.x != 0:
			_sprite.play("run")
			_sprite.flip_h = velocity.x < 0
		else:
			_sprite.play("idle")
	else:
		_sprite.play("jump")
