class_name Level
extends Resource

@export var name: String
@export var tetros: Array[Tetro]

## Return a random tetromino
func get_random(i: int) -> PackedScene:
	seed(i)
	tetros.shuffle()
	return tetros[0].scene
