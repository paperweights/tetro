extends Node2D

@export var _level: Level

@onready var _spawn_rate := $SpawnRate as Timer


func _ready():
	_spawn_rate.start()
	_spawn_rate.timeout.connect(_spawn)
	return

func _spawn():
	return
